<?php
use DWWM\Kernel\SessionManager;
use DWWM\Model\Classes\Attribution;
?>
<?php require "_head.html.php"; ?>
<?php require "_nav.html.php"; ?>
        <main role="main" class="container">
            <h1>DWWM - Session</h1>
            <h2>Group Management</h2>
<?php if($this->isConnected): ?>        
<?php if (count(SessionManager::hasPrivileges("attribution/read", true)) == 1): ?>
            <form method="post">
                <table>
                    <thead>
                        <tr>
                            <th></th>
<?php foreach($this->view_privileges as $privilege): ?>
                            <th><?= $privilege->nom ?></th>
<?php endforeach; ?>        
                        </tr>
                    </thead>
                    <tbody>
<?php foreach($this->view_groupes as $groupe): ?>
                        <tr>
                            <th><?= $groupe->nom ?></th>
<?php foreach($this->view_privileges as $privilege): ?>
<?php
$attr = new Attribution($groupe->id, $privilege->id);
$json = json_encode($attr);
$checked = in_array($json, $this->view_attributions, true);
?>
                            <td style="text-align:center;"><input type="checkbox" name="attributions[]" value="<?= "{$groupe->id}_{$privilege->id}"; ?>" <?= ($checked)?'checked="checked"':''; ?>></td>
<?php endforeach; ?>        
                        </tr>
<?php endforeach; ?>        
                    </tbody>
                </table>
                <input type="submit" name="btnSubmit" value="Mettre-à-jour" formaction="<?= $this->path; ?>/Attribution/Submit">
            </form>
<?php endif; ?>        
<?php endif; ?>        
        </main>
<?php require "_body-end.html.php"; ?>