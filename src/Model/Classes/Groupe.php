<?php
namespace DWWM\Model\Classes;

use DWWM\Model\Dao\GroupeDao;

class Groupe
{
    // Propriété(s)
    public $id;
    public $nom;

    // Association(s)
    public $privileges = [];

    // Constructeur
    public function __construct($id, $nom)
    {
        $this->id = $id;
        $this->nom = $nom;
    }

    // Méthodes statiques
    public static function getAll()
    {
        $dao = new GroupeDao();
        return $dao->getAll();
    }

    public static function getGroupesByUtilisateur($id_utilisateur)
    {
        $dao = new GroupeDao();
        return $dao->getGroupesByUtilisateur($id_utilisateur);
    }
    public static function getNotAffectedGroupesByUtilisateur($id_utilisateur)
    {
        $dao = new GroupeDao();
        return $dao->getNotAffectedGroupesByUtilisateur($id_utilisateur);
    }

}