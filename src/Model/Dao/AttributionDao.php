<?php
namespace DWWM\Model\Dao;

use \PDO;
use DWWM\Model\Dal\Dal;

class AttributionDao extends Dal
{
    private $classname = "DWWM\\Model\\Classes\\Attribution";
    private $construct_args = ['id_groupe', 'id_privilege'];

    public function getAll()
    {
        // Requête SQL
        $query = "SELECT * FROM `groupe_privilege`;";
        
        // Ouverture de connexion
        $dbh = $this->open();
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        // Execution de la requête
        $sth->execute();
        // Configuration de la récupération des résultats
        $sth->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->construct_args);
        // Récupération des résultats
        $items = $sth->fetchAll();
        // Fermeture de curseur
        unset($sth);
        // Fermeture de connexion
        unset($dbh);
        // Retour
        return $items;
    }

    public function update($values)
    {
        // Ouverture de connexion
        $dbh = $this->open();
        
        // Etape 00 : démarrage de la transaction
        $dbh->beginTransaction();

        // Etape 01 : Vider la table

        // Requête SQL
        $query = "DELETE FROM `groupe_privilege`;";
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        // Execution de la requête
        $sth->execute();
        // Fermeture de curseur
        $sth->closeCursor();

        // Etape 02 : Insérer les valeurs

        // Requête SQL
        $query = "INSERT INTO `groupe_privilege`
                  (`id_groupe`, `id_privilege`)
                  VALUES
                  ";
        $count = count($values);
        for($i = 0; $i < $count; $i++)
        {
            $query .= "(:id_groupe_{$i}, :id_privilege_{$i}),\n";
        }
        $query = substr($query, 0, -2);
        // Préparation de la requête
        $sth = $dbh->prepare($query);
        // Affectation des paramètres
        for($i = 0; $i < $count; $i++)
        {
            $sth->bindParam(":id_groupe_{$i}", $values[$i]["id_groupe"]);
            $sth->bindParam(":id_privilege_{$i}", $values[$i]["id_privilege"]);
        }
        // Execution de la requête
        $sth->execute();
        // Le dernier curseur n'est pas fermé
        //$sth->closeCursor();

        // Etape 03 : Validation de la transaction
        $dbh->commit();
        //$dbh->rollBack();

        // Fermeture de connexion
        unset($dbh);

    }
}