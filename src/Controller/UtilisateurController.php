<?php
namespace DWWM\Controller;

use DWWM\Kernel\Router;
use DWWM\Kernel\SessionManager;

use DWWM\Model\Classes\Affectation;
use DWWM\Model\Classes\Groupe;
use DWWM\Model\Classes\Utilisateur;
use DWWM\View\View;

class UtilisateurController
{
    public static function connectAction()
    {
        $login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
        $password = md5($password);

        $user = new Utilisateur(null, $login, $password);
        SessionManager::connect($user);
        $isConnected = SessionManager::isConnected();
        if ($isConnected)
        {
            // utilisateur valide
            // redirection vers la page d'accueil
            $origin = Router::getOrigin();
            $path = Router::getPath();
            header("location: http://{$origin}{$path}");
        }
        else
        {
            // utilisateur invalide
            $path = Router::getPath();
            $message = "Login or password error !";
            $groupes = [];
            $privileges = [];
            $view = new View("welcome");
            $view->bindParam("path", $path);
            $view->bindParam("message", $message);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->display();
        }
    }

    public static function disconnectAction()
    {
        SessionManager::disconnect();
        // redirection vers la page d'accueil
        $origin = Router::getOrigin();
        $path = Router::getPath();
        header("location: http://{$origin}{$path}");
    }

    public static function listAction()
    {
        if (count(SessionManager::hasPrivileges("utilisateur/read", true)) == 1)
        {
            $path = Router::getPath();
            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $users = Utilisateur::getAll();

            $view = new View("user_list");
            $view->bindParam("path", $path);
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("users", $users);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "/404";
            header("location: http://{$origin}{$path}{$options}");
        }
    }

    public static function updateAction($id)
    {
        if (count(SessionManager::hasPrivileges("utilisateur/update", true)) == 1)
        {
            $path = Router::getPath();
            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $edited_user = Utilisateur::get($id);
            $groupes_affectes = Groupe::getGroupesByUtilisateur($id);
            $groupes_non_affectes = Groupe::getNotAffectedGroupesByUtilisateur($id);
            
            $view = new View("user_update");
            $view->bindParam("path", $path);
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("edited_user", $edited_user);
            $view->bindParam("groupes_affectes", $groupes_affectes);
            $view->bindParam("groupes_non_affectes", $groupes_non_affectes);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "/404";
            header("location: http://{$origin}{$path}{$options}");
        }
    }
}