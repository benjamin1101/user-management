<?php
namespace DWWM;

use DWWM\Kernel\Route;
use DWWM\Kernel\Router;
use DWWM\Kernel\SessionManager;

require "./vendor/autoload.php";
SessionManager::start();

Router::addRoute(new Route("/", "DefaultController", "welcomeAction"));
Router::addRoute(new Route("/Welcome", "DefaultController", "welcomeAction"));
Router::addRoute(new Route("/Connect", "UtilisateurController", "connectAction"));
Router::addRoute(new Route("/Disconnect", "UtilisateurController", "disconnectAction"));
Router::addRoute(new Route("/Users", "UtilisateurController", "listAction"));
Router::addRoute(new Route("/User/{id}", "UtilisateurController", "updateAction", "", ["id" => "/^[0-9]+$/"]));
Router::addRoute(new Route("/Affect", "AffectationController", "affectAction"));
Router::addRoute(new Route("/Disaffect", "AffectationController", "disaffectAction"));
Router::addRoute(new Route("/Attribution", "AttributionController", "updateAction"));
Router::addRoute(new Route("/Attribution/Submit", "AttributionController", "submitUpdateAction"));
Router::addRoute(new Route("/404", "DefaultController", "_404Action"));
Router::addRoute(new Route("{*}", "DefaultController", "_404Action"));

$found = Router::findRoute();
if(!empty($found))
{
    $found->execute();
}
